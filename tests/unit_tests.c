/**
 * @brief      This file implements unit tests for sorting algorithms
 *
 * @author     Jeremie
 * @date       2022
 */
#include <check.h>
#include <sort_algorithms.h>
#include <stdlib.h>
#include <utils.h>

/**
 * @brief Simple test that checks whether the random number generator has
 *        has been initialized
 */
START_TEST (test_rn) { ck_assert_int_eq (init_rn_generator (), 1); }
END_TEST

Suite *sort_suite (void)
{
    Suite *s       = suite_create ("SortAlgorithms");
    TCase *tc_core = tcase_create ("Core");
    tcase_add_test (tc_core, test_rn);
    suite_add_tcase (s, tc_core);

    return s;
}

int main (void)
{
    int      no_failed = 0;
    Suite   *s         = sort_suite ();
    SRunner *runner    = srunner_create (s);
    srunner_run_all (runner, CK_NORMAL);
    no_failed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
